# name: clearance
# ---------------
# Based on idan. Display the following bits on the left:
# - Virtualenv name (if applicable, see https://github.com/adambrenecki/virtualfish)
# - Current directory name
# - Git branch and dirty state (if inside a git repo)

function _git_branch_name
  echo (command git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _git_is_dirty
  echo (command git status -s --ignore-submodules=dirty ^/dev/null)
end

function fish_prompt
  set -l last_status $status

  set -l cyan (set_color cyan)
  set -l yellow (set_color yellow)
  set -l red (set_color red)
  set -l blue (set_color blue)
  set -l green (set_color green)
  set -l normal (set_color normal)

  set -l cwd $blue(pwd | sed "s:^$HOME:~:")

  # Output the prompt, left to right

  # Print pwd or full path
  echo -n -s $cwd $normal

  # Show git branch and status
  if [ (_git_branch_name) ]
    set -l git_branch (_git_branch_name)

    if [ (_git_is_dirty) ]
      set git_info '[' $yellow $git_branch "±" $normal ']'
    else
      set git_info '[' $green $git_branch $normal ']'
    end
    echo -n -s '  ' $git_info $normal
  end

  set -l prompt_color $red
  if test $last_status = 0
    set prompt_color $normal
  end
  
  
  # Terminate with a nice prompt char
  echo -e -n -s $prompt_color ' ⟩ ' $normal
end

function fish_right_prompt
  set -l orange (set_color 9E6400)
  set -l cyan (set_color cyan)
  set -l yellow (set_color yellow)
  set -l red (set_color red)
  set -l blue (set_color blue)
  set -l green (set_color green)
  set -l normal (set_color normal)

  set -l cluster (kubectl config current-context)
  set -l ns (kubectl config view -o json | jq -r ".contexts | .[] | select (.name == \"$cluster\") | .context.namespace")

  echo -n -s '〈 '
  # Display [venvname] if in a virtualenv
  if set -q VIRTUAL_ENV
      echo -n -s  '[' $cyan (basename "$VIRTUAL_ENV") $normal ']'
  end

  echo -n -s '[' $orange $cluster $normal '|' $cyan $ns $normal ']'
end
