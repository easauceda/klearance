# Fish Theme: klearance
A forked version of clearance with k8s informational support.

A minimalist [fish shell](http://fishshell.com/) theme for people who use git and kubernetes.

![clearance theme](klearance_preview.gif)
